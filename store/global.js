import { OrdenaPosicaoDeUmArray } from "../assets/uteis"

export const state = () => ({
    
    idSiteConfig:1,
    LarguraTela:0,
    ListaCategorias:[],
    ListaSubCategorias:[],
    ListaSub2Categorias:[],

    
})

export const mutations = {
    SET_SiteConfig(state,idsiteconfig){
      state.idSiteConfig = idsiteconfig
     
    },
    SET_Categorias(state,categorias){
        state.ListaCategorias = categorias
        state.ListaCategorias.sort(OrdenaPosicaoDeUmArray);
    },
    SET_SubMenuAberto(state,params){
        var indx = state.ListaCategorias.findIndex(item => item.idcategoria === params.idcategoria);
        if (indx > -1) {
          if (state.ListaCategorias[indx].submenuaberto === true)
              state.ListaCategorias[indx].submenuaberto = false
          else
             state.ListaCategorias[indx].submenuaberto = true
          //console.log("ATUALIZOU SET Menuabertooufchado = ",params);
        }
    },
    SET_Sub2MenuAberto(state,params){
        var indx = state.ListaSubCategorias.findIndex(item => item.idsubcategoria === params.idsubcategoria);
        if (indx > -1) {
          if (state.ListaSubCategorias[indx].submenuaberto === true)
              state.ListaSubCategorias[indx].submenuaberto = false
          else
             state.ListaSubCategorias[indx].submenuaberto = true
          //console.log("ATUALIZOU SET Menuabertooufchado = ",params);
        }
    },
    SET_SubCategorias(state,subcategorias){ 
        state.ListaSubCategorias = subcategorias
        state.ListaSubCategorias.sort(OrdenaPosicaoDeUmArray);
    },
    SET_Sub2Categorias(state,sub2categorias){ 
        state.ListaSub2Categorias = sub2categorias
        state.ListaSub2Categorias.sort(OrdenaPosicaoDeUmArray);
    },
    SET_LarguraTela(state,Largura){
      state.LarguraTela = Largura
     
    },
    
}


export const actions = {
    async AtualizaSiteConfig({commit},params) {
        let RespPadrao = {erro:false}
        commit('SET_SiteConfig',params.idsiteconfig)
        return RespPadrao

    },
    async CommitLarguraTela({commit},Largura) {
        commit('SET_LarguraTela',Largura)
    }, 
    async CommitCategoria({commit},params) {
      
        commit('SET_Categorias',params.dados)
    },
    async CommitSubMenuAberto({commit},params) {
        
      commit('SET_SubMenuAberto',params)
    },
    async CommitSub2MenuAberto({commit},params) {
        
        commit('SET_Sub2MenuAberto',params)
      },
    async CommitSubCategoria({commit},params) {
      
        commit('SET_SubCategorias',params.dados)
    },
    async CommitSub2Categoria({commit},params) {
      
        commit('SET_Sub2Categorias',params.dados)
    },
    
}